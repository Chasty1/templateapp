import React, {useEffect} from 'react';
import {Button, Screen, Input} from 'ui';
import {useAuth} from 'core';
import NetInfo from '@react-native-community/netinfo';
import systemSetting from 'react-native-system-setting';

export const Login = () => {
  const {signIn} = useAuth();

  useEffect(() => {
    // listen the volume changing if you need
    const volumeListener = systemSetting.addVolumeListener(data => {
      const volume = data.value;
      console.log(volume);
    });

    return () => {
      //remove listener when you need it no more
      systemSetting.removeVolumeListener(volumeListener);
    };
  });

  useEffect(() => {
    // listen the volume changing if you need
    const airPlaneListener = systemSetting.addAirplaneListener(data => {
      systemSetting.isAirplaneEnabled().then(enable => {
        if (enable) {
          systemSetting.switchAirplane(() => {
            console.log('switch airplane successfully');
          });
        }
        const state = enable ? 'On' : 'Off';
        console.log('Current airplane is ' + state);
      });
    });
  });

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      systemSetting.isAirplaneEnabled().then(enable => {
        if (enable) {
          systemSetting.switchAirplane(() => {
            console.log('switch airplane successfully');
          });
        }
        const state = enable ? 'On' : 'Off';
        console.log('Current airplane is ' + state);
      });
    });

    return () => {
      unsubscribe();
    };
  });

  return (
    <Screen>
      <Input name="firstName" label="First Name" placeholder="Your Name" />
      <Input name="lastName" label="Last Name" placeholder="Your Last  Name" />
      <Button
        label="Login"
        onPress={() => {
          signIn('my-token-secret');
        }}
        variant="secondary"
      />
    </Screen>
  );
};
